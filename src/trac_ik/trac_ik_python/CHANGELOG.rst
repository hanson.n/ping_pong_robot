^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package trac_ik_python
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.6.2 (2021-03-17)
------------------
* changed package.xmls to format 3
* Contributors: Stephen Hart
