#!/usr/bin/env python

#-*-coding: utf-8-*-

'''
Author: Nathaniel Hanson
Date: 04/01/2021
File: ball_tracking.py

Purpose: Track ping pong ball through scene with Intel RealSense and OpenCV
'''

# Future proofing python 2
from __future__ import nested_scopes
from __future__ import generators
from __future__ import division
from __future__ import absolute_import
from __future__ import with_statement
from __future__ import print_function
from __future__ import unicode_literals
# Standard package imports
import rospy
import sys
import tf
import tf2_ros
import cv2
import roslib
import imutils
import math
import traceback
import numpy as np
from sensor_msgs.msg import Image
from geometry_msgs.msg import Pose, PointStamped, Point
from tf2_geometry_msgs import PoseStamped
from std_msgs.msg import String, Header
from cv_bridge import CvBridge, CvBridgeError
# Transformation
import tf
import tf2_ros
from tf.transformations import quaternion_from_euler, euler_from_quaternion
import geometry_msgs.msg

UPDATE_RATE = 333333 # nanoseconds

class BallTracker:
    def __init__(self):
        # setup subscribed topics
        # initialize ros/cv2 bridge
        self.bridge = CvBridge()
        self.rgb_sub = rospy.Subscriber("/camera/color/image_raw", Image, self.receive_rgb)
        self.depth_sub = rospy.Subscriber("/camera/aligned_depth_to_color/image_raw", Image, self.receive_depth)
        self.ball_pub = rospy.Publisher('/ball_pose', PointStamped, queue_size=1000)
        self.ball_global_pub = rospy.Publisher('/ball_pose/global', PointStamped, queue_size=1000)
        self.timer = rospy.Timer(rospy.Duration(0, UPDATE_RATE), self.estimate_ball)
        # Intialize empty stores for current image
        self.rgb_image = None
        self.depth_image = None
        self.buffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.buffer)
        #self.buffer.waitForTransform('camera_link', 'base_link', rospy.Time(), rospy.Duration(3))
        self.trans_parts = self.buffer.lookup_transform('camera_link','robo_base',rospy.Time(), rospy.Duration(4))
        print('Ball Tracker Initialized!')
        print(self.trans_parts)

    def receive_rgb(self, data):
        '''
        Receive RGB image from RealSense Camera
        '''
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, data.encoding)
            cv_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)
            #print('Color: {}'.format(cv_image.shape))
            self.rgb_image = cv_image
        except CvBridgeError as e:
            rospy.logerr(e)


    def receive_depth(self, data):
        '''
        Receive image from depth camera
        '''
        try:
            cv_image = cv_image = self.bridge.imgmsg_to_cv2(data, data.encoding)
            #print('Depth: {}'.format(cv_image.shape))
            self.depth_image = cv_image
        except CvBridgeError as e:
            rospy.logerr(e)

    def estimate_ball(self, timer):
        '''
        Estimate ball location in received image by register depth image to color frame
        '''
        if self.depth_image is None and self.rgb_image is None:
            return
        try:
            x, y, z = 0, 0, 0
            color_image, depth_image, x, y, z = self.detect_ball(self.rgb_image, self.depth_image)
            # Publish the update ball pose
            self.publish_ball_pose(x, y, z)
            # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
            depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

            depth_colormap_dim = depth_colormap.shape
            color_colormap_dim = color_image.shape


            images = np.hstack((color_image, depth_colormap))

            # Show images
            cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
            cv2.imshow('RealSense', images)
            cv2.waitKey(3)

        except Exception as e:
            rospy.logwarn(e)

    def publish_ball_pose(self, x, y, z):
        '''
        Publish ball coordinates in the local coordinate frame
        '''
        if sum([x, y, z]) == 0:
            return
        try:
            x, y = self.pixel_to_xy(float(x), float(y), 424, 240)
            toSend = PointStamped()
            header = Header()
            header.frame_id = 'camera_link'
            header.stamp = rospy.Time.now() 
            toSend.point.x = x
            toSend.point.y = y
            toSend.point.z = z
            toSend.header = header
            # Publish ball transformed
            self.ball_pub.publish(toSend)
            self.publish_ball_global(toSend)
        except:
            print(traceback.print_exc())

    def publish_ball_global(self, camera_point):
        try:
            pose_global = self.buffer.transform(camera_point, "robo_base")
            self.ball_global_pub.publish(pose_global)
        except Exception as e:
            pass
            #rospy.logerr(traceback.print_exc())

    def pixel_to_xy(self, px, py, im_width, im_height):
        '''
        Convert pixel coordinate from image to xy value
        '''
        # Helper function
        def rescale_pixel(OldMax, OldMin, NewMax, NewMin, OldValue):
            OldMax /= 2
            OldMin = -OldMax
            NewMax /= 2
            NewMin = - NewMax
            OldRange = (OldMax - OldMin)  
            NewRange = (NewMax - NewMin)  
            NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin
            return NewValue
        try:
            ''' Convert pixel location into xy coordinates'''
            # These values are define in relation to the global environment
            FRAME_WIDTH = 86.36 # cm
            FRAME_HEIGHT = 114.3 # cm
            x = rescale_pixel(im_width, 0, FRAME_WIDTH, 0, px)
            y = rescale_pixel(im_height, 0, FRAME_HEIGHT, 0, py)
            x -= FRAME_WIDTH / 2
            y -= FRAME_HEIGHT / 2
        except:
            print(traceback.print_exc())
        return x, y


    def detect_ball(self, frame, depth):
        '''
        Detect ball in image using HSV and round contour detection
        '''
        x, y, z = [0, 0, 0]
        blurred = cv2.GaussianBlur(frame, (11, 11), 0)
        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
        ORANGE_MIN = np.array([10, 150, 150])/255
        ORANGE_MAX = np.array([30, 255, 255])/255
        # Convert to openCV ranges
        ORANGE_MIN[0] = ORANGE_MIN[0] * 179
        ORANGE_MAX[0] = ORANGE_MAX[0] * 179
        ORANGE_MIN[1:] = ORANGE_MIN[1:] * 255
        ORANGE_MAX[1:] = ORANGE_MAX[1:] * 255

        # construct a mask for the color "green", then perform
        # a series of dilations and erosions to remove any small
        # blobs left in the mask
        mask = cv2.inRange(hsv, ORANGE_MIN, ORANGE_MAX)
        cv2.namedWindow('HSV', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('HSV', mask)
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)

        # find contours in the mask and initialize the current
        # (x, y) center of the ball
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        s = mask.shape
        blank = np.zeros((s[0],s[1],3), np.uint8)
        cv2.drawContours(blank, cnts, -1, (0,255,0), 3)
        cv2.namedWindow('Contours', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('Contours', blank)
        center = None
        valid = False
        # only proceed if at least one contour was found
        if len(cnts) > 0:
            # find the largest contour in the mask, then use
            # it to compute the minimum enclosing circle and
            # centroid
            c = self.find_best_circle(cnts)
            ((x, y), radius) = cv2.minEnclosingCircle(c)
            M = cv2.moments(c)
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

            # only proceed if the radius meets a minimum size
            if radius > 5:
                valid = True
                # draw the circle and centroid on the frame,
                # then update the list of tracked points
                cv2.circle(frame, (int(x), int(y)), int(radius),
                    (0, 255, 255), 2)
                cv2.circle(frame, center, 5, (0, 0, 255), -1)
                # Detect draw x,y,z position on the frame
                try:
                    z = self.average_ball_depth(depth, int(x), int(y), radius) / 10 # convert mm to cm
                    frame = self.write_image_text(frame, x, y, z)
                except:
                    pass
        if valid:
            return frame, depth, x, y, z
        else:
            return frame, depth, 0, 0, 0

    def write_image_text(self, img, x, y, z):
        '''
        Write image location on the image
        '''
        try:
            ball_x, ball_y = self.pixel_to_xy(float(x), float(y), 640, 480)
            font = cv2.FONT_HERSHEY_SIMPLEX
            bottomLeftCornerOfText = (10,500)
            fontScale = 0.50
            fontColor = (255,255,255)
            lineType = 2
            text = 'Ball Position ({} {} {})'.format(round(ball_x,0), round(ball_y,0), round(z,0))
            cv2.putText(img,
                text,
                (int(x)+10,int(y)+10), 
                font, 
                fontScale,
                fontColor,
                lineType)
        finally:
            return img
        
    def find_best_circle(self, contours):
        '''
        Take list of candidate contours and extract the most round contour
        to find the ball in the frame
        '''
        best_circ = 0.4
        best_contour = None
        for con in contours:
            perimeter = cv2.arcLength(con, True)
            area = cv2.contourArea(con)
            if perimeter == 0:
                break
            circularity = 4*math.pi*(area/(perimeter*perimeter))
            if circularity > best_circ:
                best_circ = circularity
                best_contour = con
        return best_contour

    def average_ball_depth(self, img, x, y, radius):
        '''
        Take ball pixel location and radius to find better estimate of ball position
        '''
        pts  = [0]
        for i in range(int(radius//4)):
            for j in range(int(radius//4)):
                try:
                    pts.append(img[y+i, x+j])
                    pts.append(img[y-i, x-j])
                    pts.append(img[y+i, x-j])
                    pts.append(img[y-i, x+j])
                except Exception as e:
                    print(e)
        return np.mean(pts)
                

if __name__ == '__main__':
    try:
        rospy.init_node('BallTracker', anonymous=True)
        processor = BallTracker()
        rospy.spin()
    except rospy.ROSInterruptException:
        rospy.logerr('Image processing node failed!')
        pass
    cv2.destroyAllWindows()