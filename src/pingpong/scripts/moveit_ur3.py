#!/usr/bin/python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
from geometry_msgs.msg import Pose, Point
from tf import transformations as tf
from math import pi
from pyquaternion import Quaternion
import numpy as np

"""
    Reads xyz position from topic /intercept_position and moves UR3
"""

class Moveit_ur3(object):

    def pos_callback(self, data):
        self.pos = [data.x, data.y, data.z]
        pose = self.set_pose([data.x, data.y, (data.z-0.05), self.quat.x, self.quat.y, self.quat.z, self.quat.w])
        pose.position.z = 0.07
        print('In callback')
        self.send_pos(pose)
        #rospy.sleep(1.0)

    def __init__(self):

        self.robot = moveit_commander.RobotCommander()

        self.scene = moveit_commander.PlanningSceneInterface()

        self.group = moveit_commander.MoveGroupCommander("manipulator")

        self.display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=10)

        self.group.set_max_velocity_scaling_factor(1)
        self.group.set_max_acceleration_scaling_factor(1)
        self.initial_pose = self.set_pose([0.171, -0.311, 0.1,  0.01116, -0.03227, -0.71767, 0.69554])
        self.quat = self.initial_pose.orientation

        print("Moving to initial position...")
        self.send_pos(self.initial_pose)
        rospy.sleep(2)
        self.position_sub = rospy.Subscriber('/intercept_position', Point, self.pos_callback)

    def set_pose(self, pose_list):
        pose = Pose()
        pose.orientation.x = pose_list[3]
        pose.orientation.y = pose_list[4]
        pose.orientation.z = pose_list[5]
        pose.orientation.w = pose_list[6]
        pose.position.x = pose_list[0]
        pose.position.y = pose_list[1]
        pose.position.z = pose_list[2]

        return pose

    # def __del__(self):
    #     moveit_commander.roscpp_shutdown()
         
    def send_pos(self, pos):
        waypoints = []
        wpose = Pose()
        wpose = pos
        wpose.orientation = self.quat
        print('Sending position: \n{}'.format(pos))

        # quat = tf.quaternion_from_euler(0, 0, 0) #send specific orientation in ypr
        # quat[0] = group.get_current_pose().pose.orientation.x
        # quat[1] = group.get_current_pose().pose.orientation.y
        # quat[2] = group.get_current_pose().pose.orientation.z
        # quat[3] = group.get_current_pose().pose.orientation.w
        # wpose.orientation.w = quat[3]
        # wpose.orientation.x = quat[0]
        # wpose.orientation.y = quat[1]
        # wpose.orientation.z = quat[2]
        waypoints.append(wpose)

        (plan3, fraction) = self.group.compute_cartesian_path(
                                    waypoints,   # waypoints to follow
                                    0.1,        # eef_step
                                    0.0)         # jump_threshold


        self.group.execute(plan3, wait=True)

    def pos_from_xyz(self, pos):
        wpose = Pose()
        wpose.position.x = pos[0]
        wpose.position.y = pos[1]
        wpose.position.z = pos[2]
        self.send_pos(wpose)

if __name__ == "__main__":
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('moveit_ur3', anonymous=True)

    bucketUR3 = Moveit_ur3()
    # x = float(sys.argv[1])
    # y = float(sys.argv[2])
    # z = float(sys.argv[3])
    # bucketUR3.pos_from_xyz([x, y, z])
    rospy.spin()
    moveit_commander.roscpp_shutdown()


##Generate joint space plan
# print "============ Generating plan 1"
# pose_target = Pose()
# quat = tf.quaternion_from_euler(0, pi/2, 0)
# pose_target.orientation.w = quat[3]
# pose_target.orientation.x = quat[0]
# pose_target.orientation.y = quat[1]
# pose_target.orientation.z = quat[2]
# pose_target.position.x = 0.2
# pose_target.position.y = -0.2
# pose_target.position.z = 0.4
# group.set_pose_target(pose_target)
# # group[0].setPlanningTime(10)
# plan1 = group.plan()

# print "============ Waiting while RVIZ displays plan1..."
# rospy.sleep(5)

## Get joint values
# group_variable_values = group.get_current_joint_values()
# print "============ Joint values: ", group_variable_values

## Set joint values
# group_variable_values[0] = 1.0
# group.set_joint_value_target(group_variable_values)
# plan2 = group.plan()
# print "============ Waiting while RVIZ displays plan2..."
# rospy.sleep(5)

# group.go(wait=True)