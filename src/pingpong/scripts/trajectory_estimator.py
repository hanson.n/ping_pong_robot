#!/usr/bin/env python

#-*-coding: utf-8-*-

'''
Author: Nathaniel Hanson
Date: 04/01/2021
File: trajectory_estimator.py

Purpose: Estimate trajectory of ball using slidng window and kalman filter
'''

# Future proofing python 2
from __future__ import nested_scopes
from __future__ import generators
from __future__ import division
from __future__ import absolute_import
from __future__ import with_statement
from __future__ import print_function
from __future__ import unicode_literals
# Standard package imports
import rospy
import sys
import tf
import tf2_ros
import roslib
import imutils
import math
import copy
import traceback
import itertools
import numpy as np
from collections import deque 
from sensor_msgs.msg import Image
from geometry_msgs.msg import Pose, PointStamped, Point
from tf2_geometry_msgs import PoseStamped
from std_msgs.msg import String, Header
# Transformation
import tf
import tf2_ros
from tf.transformations import quaternion_from_euler, euler_from_quaternion
import geometry_msgs.msg
# Visualization
from tf.transformations import quaternion_matrix
import numpy as np
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3

### GLOBAL VARIABLES
UPDATE_RATE = 333333 # nanoseconds

class TrajectoryEstimator:
    def __init__(self):
        # setup subscribed topics
        self.ball_pos_sub = rospy.Subscriber("/ball_pose/global", PointStamped, self.receive_pos)
        # setup publisher
        self.ball_pub = rospy.Publisher('/ball_traj', String, queue_size=2)
        # get the number of position to use in the window estimate
        self.positions = deque([])
        # Setup timer to remove old positions
        self.expire_t = float(rospy.get_param('/ball_estimator/expire_time')) # seconds
        self.expire_timer = rospy.Timer(rospy.Duration(self.expire_t,0), self.expire_points)
        self.clear_graph = rospy.Timer(rospy.Duration(5), self.clear_graph_func)
        # Set number of samples to use in sliding windows
        self.samples = int(rospy.get_param('/ball_estimator/samples'))
        self.extrap_period = float(rospy.get_param('/ball_estimator/forward_projection_time'))
        self.extrap_interval = float(rospy.get_param('/ball_estimator/projection_interval'))
        self.DEFORMATION_FACTOR = float(rospy.get_param('/ball_estimator/deformation_factor'))
        # Initialze fig to view points
        self.pred = []
        self.setup_plot()

    def clear_graph_func(self, timer):
        '''
        Animate graph with current data
        '''
        self.x = []
        self.y = []
        self.z = []
        self.pred = []
        self.graph._offsets3d = ([], [], [])
        self.graph_pred._offsets3d = ([], [], [])

    def setup_plot(self):
        '''
        Initialize plot to visualize ball position in world 
        '''
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, projection='3d')
        self.ax.set_xlim(-120, 120)
        self.ax.set_ylim(-120, 0)
        self.ax.set_zlim(-10, 100)
        self.ax.set_xlabel('X', fontsize=20)
        self.ax.set_ylabel('Y', fontsize=20)
        self.ax.set_zlabel('Z', fontsize=20)
        self.ax.set_title('Ball Motion', fontsize=30)
        self.x = []
        self.y = []
        self.z = []
        self.graph = self.ax.scatter(self.x, self.y, self.z)
        self.graph_pred = self.ax.scatter([], [], [])
        self.ani = FuncAnimation(self.fig, self.update_plot)
        plt.show(block=True) 

    def update_plot(self, frame):
        '''
        Animate graph with current data
        '''
        self.graph._offsets3d = (self.x, self.y, self.z)
        # Plot predicted points
        xpred = [q[0] for q in self.pred]
        ypred = [q[1] for q in self.pred]
        zpred = [q[2] for q in self.pred]

        self.graph_pred._offsets3d = (xpred, ypred, zpred)

    def receive_pos(self, data):
        '''
        Receive data from global ball position topic and parse into deque
        '''
        point = data.point
        # Keep track of points
        # Account for noise created near surface
        if point.z < 5:
            point.z = 0
        self.positions.append([point.x, point.y, point.z, data.header.stamp.secs + data.header.stamp.nsecs * 1e-9])
        self.estimate_traj()

    def bounce(self):
        pass

    def calculate_vel(self, x1, x2, t_delta):
        '''
        Calculate velocity in given coordinate frame
        '''
        return (x2 - x1) / t_delta

    def motion_model(self, positions):
        '''
        Apply ballistic trajectory model to sliding window of points
        '''
        # Estimate q_dot(0) for the first point
        g = -980.665
        t_0 = positions[0][3]
        # Estimate velocity once, since theoretically x and y vels will not change speed
        x_dot_0 = self.calculate_vel(positions[0][0], positions[-1][0], positions[-1][3] - positions[0][3])
        y_dot_0 = self.calculate_vel(positions[0][1], positions[-1][1], positions[-1][3] - positions[0][3])
        z_dot_0 = self.calculate_vel(positions[0][2], positions[2][2], positions[2][3] - positions[0][3])
        x_0 = positions[0][0] 
        y_0 = positions[0][1]
        z_0 = positions[0][2]
        t_curr = None
        z_prev = z_0 # Keep track of z so we can discard large jumps in height caused by sensor noise
        z_dot_curr = z_dot_0
        for index, val in enumerate(positions[1:]):
            t_curr = val[3]
            t_delta = t_curr - t_0
            x_new = x_dot_0 * t_delta + x_0
            y_new = y_dot_0 * t_delta + y_0
            z_new = -g/2*t_delta**2 + z_dot_0 * t_delta + z_0
            z_dot_curr += (g * t_delta)
            if z_new - z_prev > 30:
                continue
            else:
                z_prev = z_new
            if z_new < 0:
                z_new = 0
            q = np.array([x_new, y_new, z_new])
            self.pred.append(q)
        # With current track estimate in hand, we can now predict the future
        self.predict_future_path(x_new, y_new, z_new, x_dot_0, y_dot_0, z_dot_curr, t_curr)
        
        
    def predict_future_path(self, x_new, y_new, z_new, x_dot_0, y_dot_0, z_dot_curr, t_curr):
        '''
        Take existing motion model and extrapolate into the future
        '''
        # Calculate the projected paths
        t_time = t_curr
        g = -980.665
        # Run motion model for predefined time into the future
        while t_time < (t_curr + self.extrap_period):
            # Run the same motion model
            z_dot_curr_backup = z_dot_curr
            z_new_backup = z_new
            # x and y velocities do not change, so only the z needs to be updated
            z_dot_curr += (g * self.extrap_interval)
            # update position estimates
            x_new += x_dot_0 * self.extrap_interval
            y_new += y_dot_0 * self.extrap_interval
            z_new += z_dot_curr * self.extrap_interval

            # Model system inelastic bounce
            if z_new < 0:
                # Restart system at ground level
                z_new = 0
                # Calculate rebound period time
                dt_new = -z_new_backup/z_dot_curr_backup
                z_dot_curr = -(z_dot_curr_backup + g * dt_new) * self.DEFORMATION_FACTOR
                x_new += x_dot_0 * dt_new
                y_new += y_dot_0 * dt_new
            # Update our time estimate
            t_time += self.extrap_interval
            # Keep track of our prediction
            q = [x_new, y_new, z_new, t_time]
            self.pred.append(q)
        # Send new trajectory estimate
        if self.pred:
            self.publish_traj(self.pred)
    
    def publish_traj(self,data):
        '''
        Publish list of x-y-z-time estimates for ball
        '''
        t = [' '.join([str(z2) for z2 in z]) for z in data]
        toSend = '\n'.join(t)
        self.ball_pub.publish(toSend)

    def estimate_traj(self):
        '''
        If we have a sufficient number of samples, calculate the trajectory
        '''
        if len(self.positions) < self.samples:
            rospy.logwarn('Warning not enough samples!')
        else:
            self.x = [t[0] for t in self.positions]
            self.y = [t[1] for t in self.positions]
            self.z = [t[2] for t in self.positions]
            self.pred = []
            maxLen = len(self.positions)
            self.motion_model(list(itertools.islice(self.positions, maxLen - 5, maxLen)))

    def kalman_filter(self):
        '''
        Apply a Kalman Filter to smooth out incoming data measurements
        '''
        pass

    def expire_points(self, timer):
        '''
        Remove points from list if they are older than x seconds
        '''
        i = 0
        currTime = seconds = rospy.get_time()
        tPos = copy.deepcopy(self.positions)
        for point in tPos:
            if point[3] < (currTime - self.expire_t):
                i += 1
                self.positions.popleft()
        #rospy.loginfo('removed {} points, {} remaining'.format(i, len(self.positions)))
                

if __name__ == '__main__':
    try:
        rospy.init_node('TrajectoryEstimator', anonymous=True)
        processor = TrajectoryEstimator()
        rospy.spin()
    except rospy.ROSInterruptException:
        rospy.logerr('Trajectory estimation node failed!')
        pass
