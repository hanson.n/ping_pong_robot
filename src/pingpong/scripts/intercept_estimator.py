#!/usr/bin/env python

#-*-coding: utf-8-*-

'''
Author: Nathaniel Hanson
Date: 04/01/2021
File: intercept_estimator.py

Purpose: Calculate the intercept point from a set of trajectory points
'''

# Future proofing python 2
from __future__ import nested_scopes
from __future__ import generators
from __future__ import division
from __future__ import absolute_import
from __future__ import with_statement
from __future__ import print_function
from __future__ import unicode_literals
# Standard package imports
import rospy
import sys
import tf
import tf2_ros
import roslib
import imutils
import math
import copy
import traceback
import itertools
import numpy as np
from collections import deque 
from sensor_msgs.msg import Image
from geometry_msgs.msg import Pose, PointStamped, Point
from tf2_geometry_msgs import PoseStamped
from std_msgs.msg import String, Header
# Transformation
import tf
import tf2_ros
from tf.transformations import quaternion_from_euler, euler_from_quaternion
import geometry_msgs.msg
# Visualization
from tf.transformations import quaternion_matrix
import numpy as np
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3

UPDATE_RATE = 333333 # nanoseconds

class InterceptEstimator:
    def __init__(self):
        # setup subscribed topics
        self.ball_traj_sub = rospy.Subscriber('/ball_traj', String, self.receive_trajectory)
        #self.ball_traj_sub2 = rospy.Subscriber('/ball_pose/global', PointStamped, self.receive_trajectory2)
        self.pos_pub = rospy.Publisher('/intercept_position', Point, queue_size=1)
        self.traj = []
        self.tpoint = []
        self.timer = rospy.Timer(rospy.Duration(0.25), self.calculate_intercept)
        #self.timer2 = rospy.Timer(rospy.Duration(0.25), self.sendp2)

    def receive_trajectory2(self, data):
        '''
        Parse and receive ball trajectory
        '''
        point = data.point
        if -0.2 <= point.x/100 <= 0.35 and 0.2 <= point.z/100 <= 0.4:
            self.tpoint = [point.x/100, -0.35, point.z/100]

    def sendp2(self, timer):
        '''
        Parse and receive ball trajectory
        '''
        if self.tpoint:
            self.publish_point(self.tpoint)

    def receive_trajectory(self, data):
        '''
        Parse and receive ball trajectory
        '''
        traj = [z.split() for z in data.data.split('\n')]
        self.traj = traj

    def calculate_intercept(self, timer):
        '''
        Using bounds of robotic motion and predicted ball path, find a point for
        the robot to intercept the ball
        '''
        if not self.traj:
            print('No Traj!')
            return
        x_limit = [-0.2, 0.40]
        y_limit = [-0.4, -0.2]
        z_limit = [0.1, 0.5]
        foundPoint = False
        for pred in self.traj:
            print('pred')
            pred = [float(z)/100 for z  in pred]
            print(pred)
            if x_limit[0] <= float(pred[0]) <= x_limit[1] and y_limit[0] <= float(pred[1]) <= y_limit[1] and z_limit[0] <= float(pred[2]) <= z_limit[1]:
                print('Intercept target found!')
                foundPoint = True
                self.publish_point([float(z) for z in pred])
                break
        if not foundPoint:
            print('No point found')
            # For debugging purposes only!!!!
            #tPoint = np.array([0.2, -0.3, 0.2])
            #tPoint += np.random.uniform(0,0.1, 3)
            #self.publish_point(tPoint)

    def publish_point(self, data):
        '''
        Publish point for robot to move to
        '''
        toSend = Point()
        toSend.x = data[0]
        toSend.y = data[1]
        toSend.z = data[2]
        print('Sending data==========================================>')
        print(toSend)
        self.pos_pub.publish(toSend)

if __name__ == '__main__':
    try:
        rospy.init_node('InterceptEstimator', anonymous=True)
        processor = InterceptEstimator()
        rospy.spin()
    except rospy.ROSInterruptException:
        rospy.logerr('Intercept estimation node failed!')
        pass
