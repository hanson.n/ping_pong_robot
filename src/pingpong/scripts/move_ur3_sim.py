#!/usr/bin/python

#Control the robot using the /scaled_pos_joint_traj_controller/follow_joint_trajectory 
# action server (a bunch of topics), for goal the msg type is control_msgs/FollowJointTrajectoryActionGoal

import rospy
import sys
import numpy as np
import time
from tf import transformations as tf
from ur_pykdl import ur_kinematics
from sensor_msgs.msg import JointState
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from control_msgs.msg import FollowJointTrajectoryActionGoal, FollowJointTrajectoryGoal
from actionlib_msgs.msg import GoalStatusArray
from geometry_msgs.msg import Pose, PointStamped, Point

class move_ur3_sim(object):

    def joint_callback(self, data):
        self.pos = list(data.position)
        # print 'Joint angles:', self.pos, '\n'
        #ADD orientation

    def status_callback(self, data):
        if data.status_list:
            self.status = data.status_list[-1].status
            if self.status == 1:
                print('GOAL ACTIVE')
            elif self.status == 2:
                print('GOAL PREEMPTED')
            elif self.status == 3:
                print('GOAL SUCCEEDED')
        else:
            # print('No status available!')
            pass

    def ball_callback(self, data):
        ball_temp = np.array([data.point.x, data.point.y, data.point.z]).T
        R = [[0, 1, 0],
            [0, 0, -1],
            [-1, 0, 0]]
        self.ball_global = ball_temp
        # self.ball_global = np.matmul(R, ball_temp)


    def __init__(self):
        rospy.init_node('move_ur3')
        print 'Initialized move_ur3 node'
        self.count = 1
        self.init_time = rospy.get_time()
        self.status = 3
        traj_topic = '/arm_controller/follow_joint_trajectory/goal'
        goal_topic = '/arm_controller/command'

        self.ball_global = [0, 0, 0]

        self.traj_pub = rospy.Publisher(traj_topic, FollowJointTrajectoryActionGoal, queue_size=10)
        self.goal_pub = rospy.Publisher(goal_topic, JointTrajectory, queue_size=10)

        self.joint_sub = rospy.Subscriber('/joint_states', JointState, self.joint_callback)

        self.status_sub = rospy.Subscriber('/arm_controller/follow_joint_trajectory/status', GoalStatusArray, self.status_callback)
        self.kin = ur_kinematics()

        self.ball_sub = rospy.Subscriber('/ball_pose/global', PointStamped, self.ball_callback)

    
    def fkine(self, joint_angles):
        R = np.array([[1,0,0],
            [0,1,0],
            [0,0,1]]) #delete me for real robot
        pose = self.kin.forward_position_kinematics(joint_angles)
        p = np.matmul(R, pose[0:3].T)
        p = pose[0:3]
        quat = pose[3:8]
        rpy = np.matmul(R, np.array(tf.euler_from_quaternion(quat)).T)
        return [p, rpy]

    def jtraj(self, position, orientation = None, velocity = 0.1):

        alpha = 0.005
        current_pose = self.fkine(self.pos) #xyz quaternion of end effector
        joint_pose = self.pos # current joint angles
        print('Initial joint pose: {}'.format(joint_pose))

        intermediate_goals = []
        while((np.linalg.norm(np.array(position) - np.array(current_pose[0])) >= 0.01) and not rospy.is_shutdown()):
            error_pose = (np.array(position) - np.array(current_pose[0])).reshape(3,1)
            #print(error_pose)
            J = self.kin.jacobian(joint_pose)
            print(J)
            joint_change = alpha * (np.linalg.pinv(J[0:3, 0:6])) * ((error_pose / np.linalg.norm(error_pose))) * velocity
            print(joint_change)
            print(joint_pose)
            joint_pose = np.array(joint_pose)+ joint_change.reshape(1,6)
            joint_pose = joint_pose.tolist()[0]
            angles = np.array(joint_pose)
            #angles = (angles + 2 * np.pi) % (2 * np.pi)
            joint_pose = list(angles)
            current_pose = self.fkine(joint_pose)
            #dp = np.array(position) - np.array(pose_xyz[0][0:3])
            #J_inv = self.kin.jacobian_pseudo_inverse(joint_pose)
            #dq = alpha * J_inv[0:6,0:3] * (dp.reshape(3,1))

            #joint_pose = np.array(joint_pose).reshape(1,6) + dq.reshape(1,6)
            #joint_pose = joint_pose.tolist()[0]
            print('Error: {}'.format((np.linalg.norm(np.array(position) - np.array(current_pose[0])))))
            print('Joint pose: {}'.format(joint_pose))
            #pose_xyz = self.fkine(joint_pose)
            intermediate_goals.append([round(i,5) for i in joint_pose])
        return intermediate_goals
    
    def execute(self, goal_list):
        '''
        Executes a series of goal points, and waits for robot to arrive at point before continuing
        '''
        velocities = [3.141] * 6
        accelerations = [3.141] * 6
        if not rospy.is_shutdown():
            for i, goal in enumerate(goal_list):
                waiting = True
                print('Working on goal: {}/{}'.format(i, len(goal_list)))
                while waiting and not rospy.is_shutdown():
                    if self.status == 3:
                        # ur3.sendGoal(goal, velocities, accelerations)
                        ur3.sendGoalFast(goal, velocities, accelerations)
                        waiting = False

            print('Goal list succeeded!')

                
    def sendGoal(self, positions, velocities = [], accelerations = [], effort = []):
        state = JointTrajectory()
        points = JointTrajectoryPoint()
        action_goal = FollowJointTrajectoryActionGoal()
        if type(positions) != list:
            positions = positions.tolist()

        points.positions = positions
        points.velocities = velocities
        points.accelerations = accelerations
        points.effort = effort
        points.time_from_start.secs = 1
        state.joint_names = self.kin.joint_names
        state.points = [points]
        
        goal = FollowJointTrajectoryGoal()
        goal.trajectory = state
        goal.trajectory.header.stamp.secs = rospy.get_time() - self.init_time
        goal.trajectory.header.seq = self.count
        action_goal.goal = goal
        action_goal.header.seq = self.count
        # action_goal.header.stamp.secs = rospy.get_time() - init_time
        # print(goal.trajectory)
        # print("\n----\n")
        self.traj_pub.publish(action_goal)

        self.count = self.count + 1

    def sendGoalFast(self, positions, velocities = [], accelerations = [], effort = []):
        state = JointTrajectory()
        points = JointTrajectoryPoint()
        if type(positions) != list:
            positions = positions.tolist()

        points.positions = positions
        points.velocities = velocities
        points.accelerations = accelerations
        points.effort = effort
        points.time_from_start.secs = 1
        state.joint_names = self.kin.joint_names
        state.points = [points]
        
        state.header.stamp.secs = rospy.get_time()# - self.init_time
        state.header.seq = self.count
        state.header.seq = self.count

        self.goal_pub.publish(state)

        self.count = self.count + 1

    def ikine(self, goal):
        current_pose = np.array(self.kin.forward_position_kinematics(self.pos)[0:3])
        intermediate_goals_x = np.linspace(current_pose[0], goal[0]).T
        intermediate_goals_y = np.linspace(current_pose[1], goal[1]).T
        intermediate_goals_z = np.linspace(current_pose[2], goal[2]).T
        intermediate_goals = np.array([intermediate_goals_x, intermediate_goals_y, intermediate_goals_z])
        i = 0
        goal_temp = intermediate_goals[:,i]
        joint_pos = ur3.kin.inverse_kinematics(goal_temp, seed = self.pos)
        while i < (intermediate_goals.shape[1] - 1) and not rospy.is_shutdown():
            i += 1
            goal_temp = intermediate_goals[:,i]
            joint_pos = ur3.kin.inverse_kinematics(goal_temp, seed = joint_pos)    
            
        return joint_pos


if __name__ == "__main__":
    ur3 = move_ur3_sim()
    time.sleep(1)
    ur3.sendGoal(ur3.pos)
    time.sleep(0.1)

    ball = np.array([35, 0, 30])
    goal = np.true_divide(ball, 100)
    joint_pos = ur3.inverse_kinematics(goal)
    ur3.sendGoal(joint_pos)
    print(ur3.kin.forward_position_kinematics(ur3.pos))
    # while not rospy.is_shutdown():
    #     if ur3.status != 3:
    #         goal = np.true_divide(ball, 100)
    #         joint_pos = ur3.ikine(goal) #ur3.ball_global
    #         ur3.sendGoal(joint_pos)
    #         rospy.sleep(1)
    #     print(ur3.kin.forward_position_kinematics(ur3.pos))
    #     print(ur3.pos)
    #     rospy.sleep(100)


    # # w = float(sys.argv[4])
    # # i = float(sys.argv[5])
    # # j = float(sys.argv[6])
    # # k = float(sys.argv[7])
    # while not rospy.is_shutdown():
    #     print(ur3.ball_global)

    # print('Goal: {} {} {}'.format(x,y,z))
    # path = ur3.jtraj([x, y, z]) # Initial position
    # # print(path)
    # ur3.execute(path)
    # joint_pos = ur3.kin.inverse_kinematics([x, y, z], seed = ur3.pos)
    # print(ur3.kin.forward_position_kinematics(joint_pos.tolist()))
    # print(joint_pos)
    # ur3.sendGoalFast(joint_pos)
