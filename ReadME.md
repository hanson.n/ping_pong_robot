For real robot:
roslaunch ur_robot_driver ur3e_bringup.launch robot_ip:=10.75.15.209
rosrun pingpong move_ur3.py x y z

For sim:
roslaunch  ur_e_gazebo ur3e_joint_limited.launch
rosrun pingpong move_ur3.py x y z

Both:
GUI: rosrun rqt_joint_trajectory_controller rqt_joint_trajectory_controller
