# Ping Pong Robot

UR3 based kinematics and computer vision to enable robot to play ping pong.

## Clone Repository
git clone --recursive https://gitlab.com/hanson.n/ping_pong_robot.git
### Simulation Code

### Hardware Code

#### Teach Pendant

#### UR3 Connection
`roslaunch ur_robot_driver ur3_bringup.launch robot_ip:=<<ROBOT_IP>>`
#### MoveIt
`roslaunch ur3_moveit_config ur3_moveit_planning_execution.launch limited:=true`
`roslaunch ur3_moveit_config moveit_rviz.launch config:=true`
